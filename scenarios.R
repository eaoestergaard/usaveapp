################################### SCENARIO 0 START (RESET VALUES) ###################################

scenario0_numeric <- c("Annual after-tax salary"=38000,#1
                       "Growth rate of salary"=3.0,#2
                       "Valuation rate"=1.50,#3
                       "Current age"=23,#4
                       "Retirement age"=65,#5
                       "Cash and Cash Equivalents"= 5000,#6
                       "Investment Portfolio"=0,#7
                       "Vehicles"=3000,#8
                       "Housing"=0,#9
                       "Pension Portfolios"= 0,#10                   
                       "Equity in Small Business"= 0,#11
                       "Other Assets"= 3000,#12
                       "Credit Card Balance"= 0,#13
                       "Student loans"= 15000,#14
                       "Car loans"= 0,#15
                       "Mortgage Loans"= 0,#16
                       "Other Debt"=1000,#17
                       "Valuation rate"= 1.5,#18
                       "Change Of Consumption"= 3.0,#19
                       "Expected age of death"= 90,#20
                       "Beta"= 1,#21
                       "R"=1,#22
                       "Theta"= 1,#23
                       "Expected age of death model2"= 90,#24
                       "Large stocks weight"=0,#25
                       "Small stocks weight"=0,#26
                       "Corporate bonds weight"=0,#27
                       "Government bonds weight"=0,#28
                       "Financial Assets" = 11000,#29
                       "Debt" = 16000, #30
                       "Human capital" = 2157846, #31
                       
                       
                       #Optimization input
                       "Bequest Motive"= 0.25,#32
                       "Covered Yearly Salary"= 10000,#33
                       "Risk Aversion"=0,#34
                       "Time Preference"= 0.03,#35
                       "Disability Prob"=10,#36
                       "Rho"=0.05,#37
                       "relTol" = 1e-6,#38
                       "Consumption" = 11, #39
                       "Change Of Consumption" = 1, #40
                       "Large Stocks Weight" = 11, #41
                       "Small Stocks Weight" = 11, #42
                       "Corporate Bonds Weight" = 11, #43
                       "Government Bonds Weight" = 11, #44
                       
                       #Optimization input
                       "Bequest Motive"= 0.25,#32
                       "Covered Yearly Salary"= 10000,#33
                       "Risk Aversion"=0,#34
                       "Time Preference"= 0.03,#35
                       "Disability Prob"=10,#36
                       "Rho"=0.05,#37
                       "relTol" = 1e-6,#38
                       "Consumption" = 11, #39
                       "Change Of Consumption" = 1, #40
                       "Large Stocks Weight" = 11, #41
                       "Small Stocks Weight" = 11, #42
                       "Corporate Bonds Weight" = 11, #43
                       "Government Bonds Weight" = 11 #44
                       
)

scenario0_boolean<-c("Value to use financial assets "=FALSE,#1
                     "Value to use human capital"=FALSE,#2
                     "Value to use liabilities"= FALSE,#3
                     
                     "Scenario"= FALSE,#ALternative #3
                     "Incapacity Disability Coverage"= TRUE, #Yes #4
                     "Gender"= TRUE, #Female #5
                     "Pension Insurance"=TRUE, #Yes #6
                     "ConsumptionToUse"=TRUE #model1 #7
)


scenario0_global_numeric <- c("Sample period (years)"=10,#1
                              "Transaction Cost"= 0.00#2
)

scenario0_global_boolean <- c("Sample period start random or manual"=FALSE,#1
                              "Adjust for Inflation"=FALSE#2
)

scenario0_global_string <- c("Sample period start date"="2010-01-01",#1
                             "Rebalance Freqency"="years"#2
)

################################### SCENARIO 0 END (RESET VALUES) ###############################

################################### SCENARIO 1 START ############################################

scenario1_numeric <- c("Annual after-tax salary"=38000,#1
                       "Growth rate of salary"=3.0,#2
                       "Valuation rate"=1.5,#3
                       "Current age"=25,#4
                       "Retirement age"=65,#5
                       "Cash and Cash Equivalents"= 5000,#6
                       "Investment Portfolio"=0,#7
                       "Vehicles"=3000,#8
                       "Housing"=0,#9
                       "Pension Portfolios"= 0,#10                   
                       "Equity in Small Business"= 0,#11
                       "Other Assets"= 3000,#12
                       "Credit Card Balance"= 0,#13
                       "Student loans"= 15000,#14
                       "Car loans"= 0,#15
                       "Mortgage Loans"= 0,#16
                       "Other Debt"=1000,#17
                       "Valuation rate"= 1.5,#18
                       "Change Of Consumption"= 3,#19
                       "Expected age of death"= 90,#20
                       "Beta"= 1,#21
                       "R"=1,#22
                       "Theta"= 1,#23
                       "Expected age of death model2"= 90,#24
                       "Large stocks weight"=0,#25
                       "Small stocks weight"=0,#26
                       "Corporate bonds weight"=0,#27
                       "Government bonds weight"=0,#28
                       "Financial Assets" = 11000,#29
                       "Debt" = 16000, #30
                       "Human capital" = 2157846, #31
                       
                       #Optimization input
                       "Bequest Motive"= 0.25,#32
                       "Covered Yearly Salary"= 20000,#33
                       "Risk Aversion"=3,#34
                       "Time Preference"= 0.03,#35
                       "Disability Prob"=10,#36
                       "Rho"=0.05,#37
                       "relTol" = 1e-6,#38
                       "Consumption" = 20000, #39
                       "Change Of Consumption" = 1, #40
                       "Large Stocks Weight" = 25, #41
                       "Small Stocks Weight" = 25, #42
                       "Corporate Bonds Weight" = 25, #43
                       "Government Bonds Weight" = 25 #44
)

scenario1_boolean<-c("Value to use financial assets "=FALSE,#1
                     "Value to use human capital"=FALSE,#2
                     "Value to use liabilities"= FALSE,#3
                     
                     "Scenario"= FALSE,#ALternative #4
                     "Incapacity Disability Coverage"= TRUE, #Yes #5
                     "Gender"= TRUE, #Female #6
                     "Pension Insurance"=TRUE, #Yes #7
                     "ConsumptionToUse"=TRUE #model1 #8
)


scenario1_global_numeric <- c("Sample period (years)"=10,#1
                              "Transaction Cost"= 0.0#2
)

scenario1_global_boolean <- c("Sample period start random or manual"=FALSE,#1
                              "Adjust for Inflation"=FALSE,#2
                              "Scenario"= TRUE,#STD #3
                              "Incapacity Disability Coverage"= TRUE, #Yes #4
                              "Gender"= TRUE, #Female #5
                              "Pension Insurance"=TRUE, #Yes #6
                              "ConsumptionToUse"=TRUE #model1 #7
)

scenario1_global_string <- c("Sample period start date"="2010-01-01",#1
                             "Rebalance Freqency"="years"#2
)

################################### SCENARIO 1 END ############################################


################################### SCENARIO 2 START ##########################################

scenario2_numeric <- c("Annual after-tax salary"=70000,#1
                       "Growth rate of salary"=2.5,#2
                       "Valuation rate"=1.5,#3
                       "Current age"=45,#4
                       "Retirement age"=65,#5
                       "Cash and Cash Equivalents"= 30000,#6
                       "Investment Portfolio"=30000,#7
                       "Vehicles"=30000,#8
                       "Housing"=600000,#9
                       "Pension Portfolios"= 150000,#10                   
                       "Equity in Small Business"= 0,#11
                       "Other Assets"= 20000,#12
                       "Credit Card Balance"= 3500,#13
                       "Student loans"= 0,#14
                       "Car loans"= 15000,#15
                       "Mortgage Loans"= 450000,#16
                       "Other Debt"=0,#17
                       "Valuation rate"= 1.5,#18
                       "Change Of Consumption"= 3,#19
                       "Expected age of death"= 87,#20
                       "Beta"= 1,#21
                       "R"=1,#22
                       "Theta"= 1,#23
                       "Expected age of death model2"= 87,#24
                       "Large stocks weight"=70,#25
                       "Small stocks weight"=0,#26
                       "Corporate bonds weight"=0,#27
                       "Government bonds weight"=30,#28
                       "Financial Assets" = 860000,#29
                       "Debt" = 468500, #30
                       "Human capital" = 1516370, #31
                       
                       #Optimization input
                       "Bequest Motive"= 0.75,#32
                       "Covered Yearly Salary"= 20000,#33
                       "Risk Aversion"=3,#34
                       "Time Preference"= 0.03,#35
                       "Disability Prob"=10,#36
                       "Rho"=0.05,#37
                       "relTol" = 1e-6,#38
                       "Consumption" = 20000, #39
                       "Change Of Consumption" = 1, #40
                       "Large Stocks Weight" = 25, #41
                       "Small Stocks Weight" = 25, #42
                       "Corporate Bonds Weight" = 25, #43
                       "Government Bonds Weight" = 25 #44
)

scenario2_boolean<-c("Value to use financial assets "=FALSE,#1
                     "Value to use human capital"=FALSE,#2
                     "Value to use liabilities"= FALSE#3

)


scenario2_global_numeric <- c("Sample period (years)"=10,#1
                              "Transaction Cost"= 0.0#2
)

scenario2_global_boolean <- c("Sample period start random or manual"=FALSE,#1
                              "Adjust for Inflation"=FALSE,#2
                              "Scenario"= TRUE,#STD #3
                              "Incapacity Disability Coverage"= TRUE, #Yes #4
                              "Gender"= TRUE, #Female #5
                              "Pension Insurance"=TRUE, #Yes #6
                              "ConsumptionToUse"=TRUE #model1 #7
)

scenario2_global_string <- c("Sample period start date"="2010-01-01",#1
                             "Rebalance Freqency"="years"#2
)

################################### SCENARIO 2 END ############################################
