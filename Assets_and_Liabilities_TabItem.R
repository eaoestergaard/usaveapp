tabItem(
  tabName = "assetsandlilabilities",
  h2(ui_("Assets_and_Liabilities")),
  box( title = ui_("Input_Assets"), width = 6,
       
       fluidPage(
         fluidRow(
           column(2,""),
           column(10,h4(ui_("")))
         ),
         fluidRow(
           column(2,br(),h6(ui_("Cash_and_Cash_Equivalents"))),
           column(10,numericInputIcon("cashSTD", "", 0.1, min = 0, max = 1e10, step=1000, icon = icon("euro")),
                  bsTooltip("cashSTD", ui_("Cash_in_EUR")$children[[1]]))
         ),
         fluidRow(
           column(2,br(),h6(ui_("Investment_Portfolio"))),
           column(10,numericInputIcon("ipSTD", "", 0, min = 0, max = 1e10, step=1000, icon = icon("euro")),
                  bsTooltip("ipSTD", ui_("Value_of_your_investments_in_EUR")$children[[1]]))
         ),
         fluidRow(
           column(2,br(),h6(ui_("Vehicles"))),
           column(10,numericInputIcon("vehiclesSTD", "", 0, min = 0, step=1000, max = 1e10, icon = icon("euro")),
                  bsTooltip("vehiclesSTD", ui_("Value_of_your_vehicles_in_EUR")$children[[1]]))
         ),
         fluidRow(
           column(2,br(),h6(ui_("Housing"))),
           column(10,numericInputIcon("housingSTD", "", 0, min = 0, step=1000, max = 1e10, icon = icon("euro")),
                  bsTooltip("housingSTD", ui_("Value_of_real_estate_in_EUR")$children[[1]]))
         ),
         fluidRow(
           column(2,br(),h6(ui_("Pension_Portfolios"))),
           column(10,numericInputIcon("pensionSTD", "", 0, min = 0, max = 1e10, step=1000, icon = icon("euro")),
                  bsTooltip("pensionSTD", ui_("Present_value_of_your_pension_portfolios_in_EUR")$children[[1]]))
         ),
         fluidRow(
           column(2,br(),h6(ui_("Equity_in_Small_Business"))),
           column(10,numericInputIcon("equitySTD", "", 0, min = 0, max = 1e10, step=1000, icon = icon("euro")),
                  bsTooltip("equitySTD", ui_("Value_of_your_Equity_in_Small_Business_in_EUR")$children[[1]]))
         ),
         fluidRow(
           column(2,br(),h6(ui_("Other_Assets"))),
           column(10,numericInputIcon("otherAssetsSTD", "", 0, min = 0, max = 1e10, step=1000, icon = icon("euro")),
                  bsTooltip("otherAssetsSTD", ui_("Value_of_your_other_Assets_in_EUR")$children[[1]]))
         ),
         br(),
         fluidRow(
           column(2,h6("Financial_Assets")),
           column(10,verbatimTextOutput("financialAssetsCalcSTD"),
                  bsTooltip("financialAssetsCalcSTD", ui_("Total_financial_assets_in_EUR")$children[[1]]))
           
         )
       )
  ),
  box( title = ui_("Input_Liabilities"), width = 6,
       
       fluidPage(
         fluidRow(
           column(2,""),
           column(10,h4(ui_("")))
         ),
         fluidRow(
           column(2,br(),h6(ui_("Credit_Card_Balance"))),
           column(10,numericInputIcon("creditCardBalanceSTD", "", 0, min = 0, max = 1e10, step=1000, icon = icon("euro")),
                  bsTooltip("creditCardBalanceSTD", ui_("Credit_card_balance_in_EUR")$children[[1]]))
         ),
         fluidRow(
           column(2,br(),h6(ui_("Student_loans"))),
           column(10,numericInputIcon("studentLoansSTD", "", 0, min = 0, max = 1e10, step=1000, icon = icon("euro")),
                  bsTooltip("studentLoansSTD", ui_("Value_of_your_student_loans_in_EUR")$children[[1]]))
         ),
         fluidRow(
           column(2,br(),h6(ui_("Car_loans"))),
           column(10,numericInputIcon("carLoansSTD", "", 0, min = 0, max = 1e10, step=1000, icon = icon("euro")),
                  bsTooltip("carLoansSTD", ui_("Value_of_your_car_loans_in_EUR")$children[[1]]))
         ),
         fluidRow(
           column(2,br(),h6(ui_("Mortgage_loans"))),
           column(10,numericInputIcon("mortgageLoansSTD", "", 0, min = 0, max = 1e10, step=1000, icon = icon("euro")),
                  bsTooltip("mortgageLoansSTD", ui_("Value_of_your_mortgage_loans_in_EUR")$children[[1]]))
         ),
         fluidRow(
           column(2,br(),h6(ui_("Other_debt"))),
           column(10,numericInputIcon("otherDebtsSTD", "", 0, min = 0, max = 1e10, icon = icon("euro")),
                  bsTooltip("otherDebtsSTD", ui_("Value_of_your_other_debt_in_EUR")$children[[1]]))
         ),
         br(),
         fluidRow(
           column(2,h6(ui_("Implicit_liabiliities"))),
           column(10,verbatimTextOutput("implicitLiabilitiesCalcSTD"),
                  bsTooltip("implicitLiabilitiesCalcSTD", ui_("Your_total_liabilities_in_EUR")$children[[1]]))
           
         )
       )
  )
)
