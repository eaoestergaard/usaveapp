sampleStartDate <- function(investmentsdatayears = 40, dataFreq = 12){
  dates = usavePKG::totalReturn$Date
  
  maxIndex = length(dates)-dataFreq*investmentsdatayears
  if (investmentsdatayears<50) {
    minIndex = match(as.POSIXct("1965-01-01", tz = "UTC"), dates)
  } else if (investmentsdatayears<75) {
    minIndex = match(as.POSIXct("1945-01-01", tz = "UTC"), dates)
  } else {
    minIndex = 10 * dataFreq + 1 # 10 years of data is needed to calculate optimal portfolios
  }
  
  indexStart = sample(minIndex:maxIndex,1)
  samplePeriodStart = as.POSIXlt(as.Date(dates[indexStart]))
  return(samplePeriodStart)
}
