PBS<- function(
  FinancialAssetSS,
  FinancialAssetAS,
  HumanCapitalSS,
  HumanCapitalAS,
  TotalAssetsSS,
  TotalAssetsAS,
  DebtSS,
  DebtAS,
  TotalLiabilitiesSS,
  TotalLiabilitiesAS,
  EconomicNetWorthSS,
  EconomicNetWorthAS,
  EconomicNetWorthDiff
) {
  glue::glue(
    
    "
<body>
    <table cellspacing='0' border='0'>
    <colgroup width='96'></colgroup>
    <colgroup span='6' width='182'></colgroup>
    <colgroup width='96'></colgroup>
    <tr>
    <td height='41' align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    </tr>
    <tr>
    <td height='19' align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'></font></td>
    <td colspan=6 rowspan=2 align='center' valign=middle bgcolor='#f39c12'><b><font face='Arial Narrow' size=6>Personal Balance Sheet</font></b></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    </tr>
    <tr>
    <td height='19' align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    </tr>
    <tr>
    <td height='19' align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td colspan=3 rowspan=2 align='center' valign=middle bgcolor='#f39c12'><b><font face='Arial Narrow' size=4 color='#767171'>Assets</font></b></td>
    <td colspan=3 rowspan=2 align='center' valign=middle bgcolor='#f39c12'><b><font face='Arial Narrow' size=4 color='#767171'>Liabilities</font></b></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    </tr>
    <tr>
    <td height='19' align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    </tr>
    <tr>
    <td height='24' align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=4><br></font></b></td>
    <td rowspan=2 align='right' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3 color='#767171'>Scenario 1</font></b></td>
    <td rowspan=2 align='right' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3 color='#767171'>Scenario 2</font></b></td>
    <td style='border-left: 1px solid #000000' align='left' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=4 color='#767171'><br></font></b></td>
    <td rowspan=2 align='right' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3 color='#767171'>Scenario 1</font></b></td>
    <td rowspan=2 align='right' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3 color='#767171'>Scenario 2</font></b></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    </tr>
    <tr>
    <td height='24' align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=4><br></font></b></td>
    <td style='border-left: 1px solid #000000' align='left' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=4 color='#767171'><br></font></b></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    </tr>
    <tr>
    <td height='21' align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3 color='#767171'> Financial Assets</font></td>
    <td align='right' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3> {FinancialAssetSS} </font></td>
    <td align='right' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3> {FinancialAssetAS} </font></td>
    <td style='border-left: 1px solid #000000' align='left' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3 color='#767171'>  Debt</font></td>
    <td align='right' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3> {DebtSS} </font></td>
    <td align='right' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3> {DebtSS} </font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    </tr>
    <tr>
    <td height='21' align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td style='border-bottom: 1px solid #000000' align='left' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3 color='#767171'> Human Capital</font></td>
    <td align='right' style='border-bottom: 1px solid #000000' align='right' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3> {HumanCapitalSS} </font></td>
    <td  align='right' style='border-bottom: 1px solid #000000' align='right' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3> {HumanCapitalAS} </font></td>
    <td style='border-bottom: 1px solid #000000; border-left: 1px solid #000000' rowspan=1 align='left' valign=middle bgcolor='#f5c77f'><font face='Arial Narrow' size=3 color='#767171'>  Economic Net Worth</font></td>
    <td style='border-bottom: 1px solid #000000' rowspan=1 align='right' valign=middle bgcolor='#f5c77f'><font face='Arial Narrow' size=3> {EconomicNetWorthSS} </font></td>
<td style='border-bottom: 1px solid #000000' rowspan=1 align='right' valign=middle bgcolor='#f5c77f'><font face='Arial Narrow' size=3> {EconomicNetWorthAS} </font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    </tr>
    </tr>
    <tr>
    <td height='21' align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td style='border-bottom: 2px double #000000' align='left' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3 color='#767171'> Total Assets</font></b></td>
    <td style='border-bottom: 2px double #000000' align='right' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3> {TotalAssetsSS} </font></b></td>
    <td style='border-bottom: 2px double #000000' align='right' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3> {TotalAssetsAS} </font></b></td>
    <td style='border-top: 1px solid #000000; border-bottom: 2px double #000000; border-left: 1px solid #000000' align='left' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3>  Total Liabilities</font></b></td>
    <td style='border-top: 1px solid #000000; border-bottom: 2px double #000000' align='right' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3>{TotalLiabilitiesSS}</font>
<td style='border-top: 1px solid #000000; border-bottom: 2px double #000000' align='right' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3>{TotalLiabilitiesAS}</font></b></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td </td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    </tr>
    <tr>
    <td height='21' align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3 color='#767171'><br></font></b></td>
    <td align='right' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3><br></font></b></td>
    <td align='right' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3><br></font></b></td>
    <td align='center' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3><br></font></b></td>
    <td align='center' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3><br></font></b></td>
    <td align='center' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3><br></font></b></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    </tr>
    <tr>
    <td height='21' align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3 color='#767171'> Economic Net Worth</font></b></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3 color='#767171'><br></font></b></td>
    <td align='left' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3 color='#767171'><br></font></b></td>
    <td align='left' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3 color='#767171'><br></font></b></td>
    <td align='left' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3 color='#767171'><br></font></b></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    </tr>
    <tr>
    <td height='21' align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3 color='#767171'> Standard Sceanrio</font></td>
    <td align='right' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3>{EconomicNetWorthSS}</font></td>
    <td align='left' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3><br></font></td>
    <td align='left' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3><br></font></td>
    <td align='left' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3><br></font></td>
    <td align='left' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    </tr>
    <tr>
    <td height='21' align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td style='border-bottom: 1px solid #000000' align='left' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3 color='#767171'> Alternative Scenario</font></td>
    <td style='border-bottom: 1px solid #000000' align='right' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3>{EconomicNetWorthAS} </font></td>
    <td style='border-bottom: 1px solid #000000' align='left' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3><br></font></td>
    <td style='border-bottom: 1px solid #000000' align='left' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3><br></font></td>
    <td style='border-bottom: 1px solid #000000' align='left' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3><br></font></td>
    <td style='border-bottom: 1px solid #000000' align='left' valign=middle bgcolor='#FFFFFF'><font face='Arial Narrow' size=3><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    </tr>
    <tr>
    <td height='21' align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td style='border-bottom: 1px solid #000000' align='left' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3 color='#767171'> Difference</font></b></td>
    <td style='border-bottom: 1px solid #000000' align='right' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3>{EconomicNetWorthDiff}</font></b></td>
    <td style='border-bottom: 1px solid #000000' align='left' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3><br></font></b></td>
    <td style='border-bottom: 1px solid #000000' align='left' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3><br></font></b></td>
    <td style='border-bottom: 1px solid #000000' align='left' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3><br></font></b></td>
    <td style='border-bottom: 1px solid #000000' align='left' valign=middle bgcolor='#FFFFFF'><b><font face='Arial Narrow' size=3><br></font></b></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    </tr>
    <tr>
    <td height='19' align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    <td align='left' valign=bottom bgcolor='#FFFFFF'><font color='#000000'><br></font></td>
    </tr>
    </table>
    <!-- ************************************************************************** -->
    </body>
  
  
    "
  )
}
