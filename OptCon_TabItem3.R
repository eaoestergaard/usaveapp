tabItem(
  tabName = "optCon3",
  h2(ui_("Optimal_Consumption")),
  box( title = ui_("Input_Optimal_Consumption"), width = 6,
       
       fluidPage(
         fluidRow(
           
           
           column(4, radioButtons("optPensionInsurance3", ui_("optPensionInsurance"),choiceNames = list(ui_("Yes"),ui_("No")),choiceValues= list(FALSE,TRUE),selected =TRUE)), #YES = FALSE because its mapped to longevityRisk
           bsTooltip("optPensionInsurance3", ui_("PensionInsurance")$children[[1]]),
           
           column(4,sliderInput("optBequestMotive3", label=ui_("Bequest_motive"), value=0.75, min = 0, max = 1, step = 0.05)),
           bsTooltip("optBequestMotive3", ui_("Relative_weight_of_bequest_utility")$children[[1]]),
           
           #column(2, switchInput(inputId = "optPensionInsurance", label=ui_("optPensionInsurance"), value = FALSE, onLabel = ui_("Yes"),offLabel = ui_("No"),onStatus = "warning")),
           
           #column(2, switchInput(inputId = "optIncapacityDisabilityCoverage", label=ui_("optIncapacityDisabilityCoverage"), value = FALSE, onLabel = ui_("Yes"),offLabel = ui_("No"),onStatus = "warning")),
           #column(2, radioButtons("optIncapacityDisabilityCoverage", ui_("optIncapacityDisabilityCoverage"),choiceNames = list(ui_("Yes"),ui_("No")),choiceValues= list(TRUE,FALSE),selected =TRUE)),
           #bsTooltip("optIncapacityDisabilityCoverage", ui_("IncapacityDisabilityCoverage")$children[[1]]),
           
           #column(2,radioButtons("optDisabilityProb", label=ui_("Probability_of_disability"),choiceNames = list(ui_("Low"),ui_("Medium"), ui_("High")),choiceValues= c(10,25,40),selected =10)),
           column(4,sliderInput("optTimePreference3", label=ui_("Time_preference"), value=0.01, min = 0, max = 0.1, step = 0.005)),
           bsTooltip("optTimePreference3", ui_("Time_preference_parameter")$children[[1]])
           
         ),
         fluidRow(    
           #column(2, switchInput(inputId = "optScenario", label=ui_("optScenario"), value = FALSE, onLabel = ui_("Standard"),offLabel = ui_("Alternative"),onStatus = "warning",width = '1000%')),
           column(4, radioButtons("optScenario3", ui_("optScenario"),choiceNames = list(ui_("Standard"),ui_("Alternative")),choiceValues= list(TRUE,FALSE),selected =TRUE)),
           bsTooltip("optScenario3", ui_("Scenario_selection")$children[[1]]),
           
           #column(2, switchInput(inputId = "gender", label=ui_("gender"), value = FALSE, onLabel = ui_("Female"),offLabel = ui_("Male"),onStatus = "warning")),
           column(4, radioButtons("gender3", ui_("gender"),choiceNames = list(ui_("Female"),ui_("Male")),choiceValues= list(TRUE,FALSE),selected =TRUE)),
           bsTooltip("gender3", ui_("Male_or_female")$children[[1]]),
           
           column(4,sliderInput("optRiskAversion3", label=ui_("Risk_aversion"), value=3, min = 0, max = 30, step = 1)),
           bsTooltip("optRiskAversion3", ui_("Level_of_risk_aversion")$children[[1]]),
         ),
         fluidRow(
           column(4,radioButtons("optDisabilityProb3", label=ui_("Probability_of_disability"),c("Low" = 10,"Medium" = 25,"High" = 40),selected =10)),
           bsTooltip("optDisabilityProb3", ui_("Probability_of_disability_that")$children[[1]]),
           
           column(4,numericInputIcon("optCoveredYearlySalary3", ui_("optCoveredYearlySalary"), 0, min = 0, max = 9999999999, step=1000, icon = icon("euro"))),
           bsTooltip("optCoveredYearlySalary3", ui_("Covered_Yearly_Salary")$children[[1]]),
           
           #column(2,switchInput(inputId = "optConsumptionToUse", label=ui_("optConsumptionToUse"), value = FALSE, onLabel = ui_("Model1"),offLabel = ui_("Model2"),onStatus = "warning",width = '1000%')),
           column(4, radioButtons("optConsumptionToUse3", ui_("optConsumptionToUse"),choiceNames = list(ui_("Model1"),ui_("Model2")),choiceValues= list(TRUE,FALSE),selected =TRUE)),
           bsTooltip("optConsumptionToUse3", ui_("ConsumptionModel")$children[[1]]),
           
           column(2,sliderInput("optRho3", label=ui_("Rho"), value=0.05, min = 0, max = 0.1, step = 0.005)),
           bsTooltip("optRho3", ui_("conversion_factor_for_lifelong_pension")$children[[1]])
           
         )
       ),
       fluidRow(
         column(2,""),
         column(6,numericInputIcon("relTol3", label="REL.Tol (TEST ONLY)", value=1e-6, min = 1e-16, max = 1))
       ),
       fluidRow(id = 'Optconheader_fluidRow3',
                column(3,""),
                column(4,h4(ui_("Input"))),
                column(4,h4(ui_("Optimized_values")))
       ),
       fluidRow(id = 'Consumption_fluidRow3',
                column(3,br(),h6(ui_("Consumption"))),
                column(4,numericInputIcon("optConsumptionCalc3", "", 0, min = 0, max = 1e10, step=1000, icon = icon("euro")),
                       bsTooltip("optConsumptionCalc3", ui_("Consumption_used_in_optimization")$children[[1]])),
                br(),
                column(4,verbatimTextOutput("optConsumptionOptimal3"),
                       bsTooltip("optConsumptionOptimal3", ui_("Optimal_consumption")$children[[1]]))
       ),
       fluidRow(id = 'Change_Of_Consumption_fluidRow3',
                column(3,br(),h6(ui_("Change_Of_Consumption"))),
                column(4,numericInputIcon("optChangeOfConsumptionCalc3", "", 0, min = -100, max = 100, step=0.5, icon = icon("percent")),
                       bsTooltip("optChangeOfConsumptionCalc3", ui_("Change_of_consumption_used_in_optimization")$children[[1]])),
                br(),
                column(4,verbatimTextOutput("optChangeOfConsumptionOptimal3"),
                       bsTooltip("optChangeOfConsumptionOptimal3", ui_("Optimal_change_of_consumption")$children[[1]]))
       ),
       fluidRow(id = 'Large_stocks_weight_fluidRow3',
                column(3,br(),h6(ui_("Large_stocks_weight3"))),
                column(4,numericInputIcon("optLargeStocksWeightCalc3", "", 0, min = 0, max = 100, step=0.5, icon = icon("percent")),
                       bsTooltip("optLargeStocksWeightCalc3", ui_("Large_stocks_weight_used_in_optimization")$children[[1]])),
                br(),
                column(4,verbatimTextOutput("optLargeStocksWeightOptimal3"),
                       bsTooltip("optLargeStocksWeightOptimal3", ui_("Optimal_weight")$children[[1]]))
       ),
       fluidRow(id = 'Small_stocks_weight_fluidRow3',
                column(3,br(),h6(ui_("Small_stocks_weight"))),
                column(4,numericInputIcon("optSmallStocksWeightCalc3", "", 0, min = 0, max = 100, step=0.5, icon = icon("percent")),
                       bsTooltip("optSmallStocksWeightCalc3", ui_("Large_stocks_weight_used_in_optimization")$children[[1]])),
                br(),
                column(4,verbatimTextOutput("optSmallStocksWeightOptimal3"),
                       bsTooltip("optSmallStocksWeightOptimal3", ui_("Optimal_weight")$children[[1]]))
                
       ),
       fluidRow(id = 'Corporate_bonds_weight_fluidRow3',
                column(3,br(),h6(ui_("Corporate_bonds_weight"))),
                column(4,numericInputIcon("optCorporateBondsWeightCalc3", "",0, min = 0, max = 100, step=0.5, icon = icon("percent")),
                       bsTooltip("optCorporateBondsWeightCalc3", ui_("Corporate_bonds_weight_used_in_optimization")$children[[1]])),
                br(),
                column(4,verbatimTextOutput("optCorporateBondsWeightOptimal3"),
                       bsTooltip("optCorporateBondsWeightOptimal3", ui_("Optimal_weight")$children[[1]]))
       ),
       fluidRow(id = 'Government_bonds_weight_fluidRow3',
                column(3,br(),h6(ui_("Government_bonds_weight"))),
                column(4,numericInputIcon("optGovernmentBondsWeightCalc3", "", 0, min = 0, max = 100, step=0.5, icon = icon("percent")),
                       bsTooltip("optGovernmentBondsWeightCalc3", ui_("Government_bonds_weight_used_in_optimization")$children[[1]])),
                br(),
                column(4,verbatimTextOutput("optGovernmentBondsWeightOptimal3"),
                       bsTooltip("optGovernmentBondsWeightOptimal3", ui_("Optimal_weight")$children[[1]]))
       ),
       
       br(),
       fluidRow(
         column(3,actionButton("OptimizeConsumption3", ui_("Optimize"),icon = icon("calculator"), style='padding:15px; font-size:125%; background-color:orange')),
         column(4,actionButton("optCopyOptToMan3", ui_("Copy_optimal_values"),icon = icon("copy"), style='padding:15px; font-size:125%; background-color:grey'))
         
       )
  ),
  box(id="optimalConsumptionPlotBox",width=6,title = "",collapsible = TRUE, collapsed = FALSE,
      fluidPage(
        column(12,h4(ui_("Comparing_Optimal_vs_selfselected_input_values"), align = 'center')),
        column( width = 6, withSpinner(highchartOutput('optimalConsumptionBarPlot13'), color = "orange" )),
        column( width = 6, withSpinner(highchartOutput('optimalConsumptionBarPlot23'), color = "orange" )),
        column( width = 6, withSpinner(highchartOutput('optimalConsumptionBarPlot33'), color = "orange" )),
        column( width = 6, withSpinner(highchartOutput('optimalConsumptionBarPlot43'), color = "orange" )),
        
        
      )
  ),
  box(id="optimalConsumptionPathPlotBox3",width=12,title = "",collapsible = TRUE, collapsed = FALSE,
      fluidPage(
        column( width = 12,h4(ui_("Consumption_path_Average_total_optimal_consumption_25_confidence_interval_consumption_extreme_consumption_paths_and_consumption_based_on_input_values"), align = 'center'), 
                withSpinner(highchartOutput('optimalConsumptionPathPlot3'), color = "orange" )),
      )
  ),
  box(id="optimalWealthPathPlotBox3",width=12,title = "",collapsible = TRUE, collapsed = FALSE,
      fluidPage(
        column( width = 12,h4(ui_("Financial_Wealth_Path_Average_optimal_wealth_confidence_interval_wealth_extreme_wealth_paths_and_wealth_based_on_input_values"), align = 'center'), 
                withSpinner(highchartOutput('optimalWealthPathPlot3'), color = "orange" )),
      )
  )
)

